

open


Initiate pairing for the headset by pressing and holding for five seconds the Volume + and Next track/ Fast forward buttons on the headset. Next, initiate paring for your Bluetooth device. (Consult the documentation that comes with your Bluetooth device.) Try using the headset with a different Bluetooth device.

